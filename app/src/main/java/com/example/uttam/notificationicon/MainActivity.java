package com.example.uttam.notificationicon;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
//
//import com.google.android.gms.appinvite.AppInviteInvitation;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.OnFailureListener;
//import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.analytics.FirebaseAnalytics;
//import com.google.firebase.appinvite.FirebaseAppInvite;
//import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
//import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
//import com.google.firebase.dynamiclinks.ShortDynamicLink;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    Button b1;

//    private final String TAG = getClass().getName();
//    private FirebaseAnalytics analytics;
//    private int Request_Code=11;
    String action;
    Uri data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        Intent intent=getIntent();
//        if(intent!=null && intent.getData()!=null) {
//            action = intent.getAction();
//            data = intent.getData();
//        }

        b1 = (Button)findViewById(R.id.notificationBtnId);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this,"Not adding yet",Toast.LENGTH_SHORT).show();
                addNotification();
            }
        });

    }

//    public void invitation(View view){
//        Intent intent = new AppInviteInvitation.IntentBuilder("Invitation Title")
//                .setMessage("Just check one time")
//                .setDeepLink(data)
//                .setCallToActionText("Invitation")
//                .build();
//        startActivityForResult(intent, Request_Code);
//    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);
//
//        if (requestCode == Request_Code) {
//            if (resultCode == RESULT_OK) {
//                // Get the invitation IDs of all sent messages
//                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
//                for (String id : ids) {
//                    Log.d(TAG, "onActivityResult: sent invitation " + id);
//                }
//            } else {
//                // Sending failed or it was canceled, show failure message to the user
//                // ...
//            }
//        }
//    }


    private void addNotification() {

       // int m=1;
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;

        Log.d("T", "Displaying Notification");
        Intent activityIntent = new Intent(this, NotificationView.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.synopi_icon_512);
        mBuilder.setColor(Color.GREEN);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setContentTitle("My Notification");
        mBuilder.setContentText("Content of Notification");
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        //mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        mBuilder.setAutoCancel(true);

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(m, mBuilder.build());
    }
}

